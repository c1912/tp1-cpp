#include <iostream>
#include <time.h>
#include <conio2.h>

using namespace std;

int resultat, nbr1, nbr2, nbrinter, number3, temp, taille, ordre;


int addition(int a, int b)
{
    resultat = a + b;

    return resultat;
}

int somme3p(int nbsp1, int nbsp2, int *nbsp3)
{
    nbsp3 = nbsp1 + nbsp2;
    return(nbsp3);
}

int somme3(int nbs1, int nbs2, int nbs3)
{
    nbs3 = nbs1 + nbs2;

    return nbs3;
}



int main()
{
    cout << "nbr1 : ";
    cin >> nbr1;
    cout << "nbr2 : ";
    cin >> nbr2;
    addition(nbr1, nbr2);
    cout << "La somme des nombres est : " << resultat << endl;
    _getch();


    nbrinter = nbr1;
    nbr1 = nbr2;
    nbr2 = nbrinter;
    cout << "nbr1 vaut : " << nbr1 << " et nbr2 vaut : " << nbr2 << " apres changement." << endl;
    _getch();

    cout << "Le 3eme nombre avec pointeur vaut : " << somme3p(nbr1, nbr2, &number3); << endl;
    getch();

    cout << "Le 3eme nombre vaut : " << somme3(nbr1, nbr2, number3) << endl;
    _getch();
    clrscr();


    cout << "Veuillez saisir la taille du tableau : ";
    cin >> taille;
    int tableau[taille];

    cout << endl << "Tableau :" << endl;
    srand((unsigned int)time(0));
    for (int i = 0; i < taille; i++)
    {
        int valeur = rand();
        tableau[i] = valeur;
        cout << tableau[i] << endl;

    }
    for(int j = 1; j <= taille; j++)
    {
        for(int i = 0; i < taille-1 ; i++)
        {
            if(tableau[i]>tableau[i+1])
            {
                temp = tableau[i];
                tableau[i] = tableau[i+1];
                tableau[i+1] = temp;
            }
        }
    }

    cout << endl << "Afficher le tableau par ordre croissant ou decroisssant ? (0 pour croissant ou 1 pour decroissant) : ";
    cin >> ordre;

    if(ordre == 0)
    {
        cout << endl << "Tableau par ordre croissant :" << endl;
        for(int i = 0; i < taille ; i++)
        {
            cout << tableau[i] << endl;
        }

        cout << endl << "Tableau par ordre decroissant :" << endl;
        for(int i = taille-1; i >= 0; i--)
        {
            cout << tableau[i] << endl;
        }
    }
    else
    {
        cout << endl << "Tableau par ordre decroissant :" << endl;
        for(int i = taille-1; i >= 0; i--)
        {
            cout << tableau[i] << endl;
        }

        cout << endl << "Tableau par ordre croissant :" << endl;
        for(int i = 0; i < taille ; i++)
        {
            cout << tableau[i] << endl;
        }
    }

    return 0;
}

