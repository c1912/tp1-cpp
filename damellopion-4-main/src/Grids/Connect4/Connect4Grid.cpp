//
// Created by Fyzoriel on 26/10/2021.
//

#include "Connect4Grid.h"

Connect4Grid::Connect4Grid() : GridLine(4,7,4){}

int Connect4Grid::getFirstEmptyCell(const int column) const
{
    for (int i = _lines - 1; i >= 0; i--)
    {
        if (isEmptyCell(column, i))
        {
            return i;
        }
    }
    return -1;
}

bool Connect4Grid::hasEmptyCell(const int column) const
{
    for (int i = _lines - 1; i >= 0; i--)
    {
        if (isEmptyCell(column, i))
        {
            return true;
        }
    }
    return false;
}

