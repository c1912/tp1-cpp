//
// Created by Fyzoriel on 26/10/2021.
//

#include "src/Player/Player.h"
#include "src/Cell/Cell.h"
#include "src/Grids/Grid.h"
#include "src/Grids/GridLine.h"

#ifndef DAMELLOPION4_CONNECT4GRID_H
#define DAMELLOPION4_CONNECT4GRID_H

class Connect4Grid : public GridLine
{

public:
    Connect4Grid();
    bool hasEmptyCell(int column) const;
    int getFirstEmptyCell(int column) const;
};

#endif //DAMELLOPION4_CONNECT4GRID_H