//
// Created by Fyzoriel on 10/11/2021.
//

#ifndef DAMELLOPION4_GRIDLINE_H
#define DAMELLOPION4_GRIDLINE_H

#include "src/Player/Player.h"
#include "Grid.h"

class GridLine : public Grid
{
public:
    GridLine(int x, int y, int tokenInLine);

    ~GridLine() override = default;
 /**
 * Add token to the grid
 * @param player The player who add the token
 */
    void addToken(const Player &player, std::pair<int, int> move);
 /**
 * Check if a player is the winner
 * @param player The player we want to check the win
 * @return true if player is the winner
 */
    bool isWinner(const Player &player);

private:
    const int _tokenInLine;
 /**
 * Check if a line has been complete by a player according to the number of cells in row needed
 * @param line The number of the line to check
 * @param player The player we want to check if he has complete the line
 * @return true if line is completed
 */
    bool isLineComplete(int line, const Player &player);
 /**
 * Check if a column has been complete by a player according to the number of cells in row needed
 * @param column The number of the column to check
 * @param player The player we want to check if he has complete the column
 * @return true if column is completed
 */
    bool isColumnComplete(int column, const Player &player);
 /**
 * Check if at least one diagonal has been complete by a player according to the number of cells in row needed
 * @param player The player we want to check if he has complete the column
 * @return true if at least one diagonal is completed
 */
    bool isDiagonalComplete(const Player &player);
};


#endif //DAMELLOPION4_GRIDLINE_H
