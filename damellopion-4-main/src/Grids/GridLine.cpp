//
// Created by Fyzoriel on 10/11/2021.
//

#include "GridLine.h"

GridLine::GridLine(int x, int y, int tokenInLine) : Grid(x, y), _tokenInLine(tokenInLine){}

bool GridLine::isLineComplete(int line, const Player &player)
{
    int lineCounter = 0;
    for (int x = 0; x < _columns; x++)
    {
        if (_grid[line][x].getValue() == player.getValue())
        {
            lineCounter++;
        }
        else
        {
            lineCounter = 0;
        }

        if (lineCounter == _tokenInLine)
        {
            return true;
        }
    }
    return false;
}

bool GridLine::isColumnComplete(int column, const Player &player)
{

    int lineCounter = 0;
    for (int y = _lines - 1; y >= 0; y--)
    {
        if (_grid[y][column].getValue() == player.getValue())
        {
            lineCounter++;
        }
        else
        {
            lineCounter = 0;
        }

        if (lineCounter == _tokenInLine)
        {
            return true;
        }
    }

    return false;
}

bool GridLine::isDiagonalComplete(const Player &player)
{
    const char color = player.getValue();

    //   3
    //  2
    // 1
    // for each column, line, check diagonal (from bottom left to top right), Start at bottom left

    // y = max l size - 1
    // y limit = max l size - 1 - (max l size - line size + 1)
    // x = 0
    // x limit = max c size - line size + 1
    for (int y = _lines - 1; y > _lines - 2; y--)
    {
        for (int x = 0; x <= _columns - _tokenInLine; x++)
        {
            int lineCounter = 0;
            for (int i = 0; i < _tokenInLine; i++)
            {
                if (_grid[y - i][x + i].getValue() == color)
                {
                    lineCounter++;
                }
                if (lineCounter == _tokenInLine)
                {
                    return true;
                }
            }
        }
    }

    // 3
    //  2
    //   1
    // for each column, line, check diagonal (from bottom right to top left), Start at bottom right

    // y = max l size - 1
    // y limit = line size - 2
    // x = max c size - 1
    // x limit = max c size - line size
    for (int y = _lines - 1; y > _lines - 2; y--)
    {
        for (int x = _columns - 1; x >= _columns - _tokenInLine; x--)
        {
            int cellNumber = 0;
            for (int i = 0; i < _tokenInLine; i++)
            {
                if (_grid[y - i][x - i].getValue() == color)
                {
                    cellNumber++;
                }
                if (cellNumber == _tokenInLine)
                {
                    return true;
                }
            }
        }
    }

    return false;
}

bool GridLine::isWinner(const Player &player)
{
    for (int i = 0; i < _columns; i++)
    {
        if (isColumnComplete(i, player))
        {
            return true;
        }
    }

    for (int i = 0; i < _lines; i++)
    {
        if (isLineComplete(i, player))
        {
            return true;
        }
    }

    return isDiagonalComplete(player);
}

void GridLine::addToken(const Player &player, std::pair <int,int> move)
{
    int x = move.first;
    int y = move.second;
    _grid[y][x].setValue(player.getValue());
}

