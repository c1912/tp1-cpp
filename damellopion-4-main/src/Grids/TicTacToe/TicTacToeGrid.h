/**
 * Header file of the _grid for the tic-tac-toe game
 * @file TicTacToeGrid.h
 * @author Fyzoriel
 * @date 2021/10/13
 */

#ifndef DAMELLOPION4_TICTACTOEGRID_H
#define DAMELLOPION4_TICTACTOEGRID_H

#include <vector>
#include "src/Games/Game.h"
#include "src/Player/Player.h"
#include "src/Cell/Cell.h"
#include "src/Grids/Grid.h"
#include "src/Grids/GridLine.h"

class TicTacToeGrid : public GridLine
{

public:
    TicTacToeGrid();
};


#endif //DAMELLOPION4_TICTACTOEGRID_H
