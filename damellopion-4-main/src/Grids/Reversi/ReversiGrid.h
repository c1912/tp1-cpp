//
// Created by ben on 21/11/2021.
//

#ifndef DAMELLOPION4_REVERSIGRID_H
#define DAMELLOPION4_REVERSIGRID_H

#include "src/Grids/Grid.h"

class ReversiGrid : public Grid
{
public:
    ReversiGrid(char color1, char color2);
 /**
 * Init the grid vector and place default token
 */
    void initArray();
 /**
 * Add token to the Tic Tac Toe grid
 * @param player The player who add the token
 */
    void addToken(const Player& player);
 /**
 * Recursively check if a token can be placed and if flip mode is true place/flip token in only one direction
 * @param player The player who want to place the token
 * @param x The x coordinate
 * @param y The y coordinate
 * @param xModifier The yModifier use for the direction
 * @param yModifier The xModifier use for the direction
 * @param first If this is the first entry in the function
 * @param flipMode The flip mode (If true flip tokens else just check)
 * @return true if can place token / has flip tokens
 */
    bool checkFlipToken(const Player& player, int x, int y, int xModifier, int yModifier, bool first, bool flipMode);
 /**
 * Check if a token can be placed and if flip mode is true place/flip token
 * @param player The player who want to place the token
 * @param x The x coordinate
 * @param y The y coordinate
 * @param flipMode The flip mode (If true flip tokens else just check)
 * @return true if can place token / has flip tokens
 */
    bool checkFlipToken(const Player& player, int x, int y, bool flipMode);
 /**
 * Check if the player can play
 * @param player The player who want to check the possibility to play
 * @return true if player can play
 */
    bool canPlay(const Player& player);
 /**
 * Change int references passed has argument for score values
 * @param scorePlayer1 The reference that take the score of the player 1
 * @param scorePlayer2 The reference that take the score of the player 2
 * @param scoreNeutral The reference that take the score of neutral cells
 */
    void getScores(int& scorePlayer1, int& scorePlayer2, int& scoreNeutral);
private:
    char _colorPlayer1;
    char _colorPlayer2;
};
#endif //DAMELLOPION4_REVERSIGRID_H