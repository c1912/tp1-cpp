//
// Created by boybe on 21/11/2021.
//

#include <iostream>

#include "src/Utils/AskUser.h"
#include "ReversiGrid.h"
#include "src/Player/Player.h"
#include "src/Utils/Display.h"

ReversiGrid::ReversiGrid(char color1, char color2) : Grid(8,8), _colorPlayer1(color1), _colorPlayer2(color2)
{
    initArray();
}

bool ReversiGrid::checkFlipToken(const Player &player, int x, int y, bool flipMode)
{
    bool hasFlip = false;
    for (int i = 0; i < 8; i++)
    {
        // Change direction modifier
        std::vector<std::vector<int>> directionModifier = {
                {-1, -1}, {-1, 0}, {-1, 1}, // 3 positions above
                {0, -1}, {0, 1}, // 2 positions on same row
                {1, -1}, {1, 0}, {1, 1} // 3 positions below
        };

        if (checkFlipToken(player, x, y, directionModifier[i][0], directionModifier[i][1], true, flipMode))
        {
            hasFlip = true;
        }
    }
    if (hasFlip && flipMode)
    {
        _grid[y][x].setValue(player.getValue());
    }

    return hasFlip;
}

bool ReversiGrid::checkFlipToken(const Player &player, int x, int y, int xModifier, int yModifier, bool first, bool flipMode)
{
    int nextX = x + xModifier;
    int nextY = y + yModifier;

    if (nextX < 0 || nextX >= _columns
        || nextY < 0 || nextY >= _lines
        || _grid[nextY][nextX].getValue() == ' ')
    {
        return false;
    }

    if (_grid[nextY][nextX].getValue() == player.getValue())
    {
        // If first return false (The same color side by side), otherwise return true
        return !first;
    }

    bool canFlip = checkFlipToken(player, nextX, nextY, xModifier, yModifier, false, flipMode);
    if (canFlip && flipMode)
    {
        _grid[nextY][nextX].setValue(player.getValue());
    }
    return canFlip;
}

void ReversiGrid::addToken(const Player &player)
{
    int x, y;
    bool error;
    do
    {
        x = AskUser::askNumber(_columns);

        y = AskUser::askNumber(_lines);

        error = !isEmptyCell(x, y) || !checkFlipToken(player, x, y, true);
        if (error)
        {
            Display::invalidPosition();
        }

    } while (error);
}

void ReversiGrid::initArray()
{
    _grid[3][3].setValue(_colorPlayer2);
    _grid[4][4].setValue(_colorPlayer2);
    _grid[3][4].setValue(_colorPlayer1);
    _grid[4][3].setValue(_colorPlayer1);
}

bool ReversiGrid::canPlay(const Player &player)
{
    for (int x = 0; x < _columns; x++)
    {
        for (int y = 0; y < _lines; y++)
        {
            if (checkFlipToken(player, x, y, false))
            {
                return true;
            }
        }
    }
    return false;
}

void ReversiGrid::getScores(int &scorePlayer1, int &scorePlayer2, int &scoreNeutral)
{
    int player1 = 0, player2 = 0, neutral = 0;
    for (int x = 0; x < _columns; x++)
    {
        for (int y = 0; y < _lines; y++)
        {
            if (_grid[y][x].getValue() == _colorPlayer1)
            {
                player1++;
            }
            else if (_grid[y][x].getValue() == _colorPlayer2)
            {
                player2++;
            }
            else
            {
                neutral++;
            }
        }
    }
    scorePlayer1 = player1;
    scorePlayer2 = player2;
    scoreNeutral = neutral;
}
