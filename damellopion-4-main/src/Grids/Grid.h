//
// Created by Fyzoriel on 26/10/2021.
//

#ifndef DAMELLOPION4_GRID_H
#define DAMELLOPION4_GRID_H

#include <vector>
#include "src/Player/Player.h"
#include "src/Cell/Cell.h"

class Grid
{
public:
    Grid(int lines, int columns);
    virtual ~Grid() = default;

    inline int getColumns() const { return _columns; }
    inline int getLines() const { return _lines; }
    inline const std::vector<std::vector<Cell>>& getGrid() { return _grid; };
 /**
 * Display the grid with coordinate number and cell values
 */
    void displayGrid();
 /**
 * Check if cell is empty
 * @param x The x coordinate
 * @param y The y coordinate
 * @return true if cell is empty
 */
    bool isEmptyCell(int x, int y) const;
 /**
 * Check if the grid is full
 * @return true if all cells have values
 */
    bool isFull();
 /**
 * Init the grid vector
 */
    void initArray();

    void load(const std::vector<std::vector<Cell>>& grid);

protected:
    std::vector<std::vector<Cell>> _grid;
    const int _columns;
    const int _lines;
};


#endif //DAMELLOPION4_GRID_H
