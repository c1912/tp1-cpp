//
// Created by Fyzoriel on 26/10/2021.
//

#include "Grid.h"
#include <vector>
#include <iostream>
#include "src/Utils/Display.h"

Grid::Grid(int lines, int columns) : _lines(lines), _columns(columns)
{
    initArray();
}

void Grid::displayGrid()
{
    Display::displayGrid(_grid, _lines, _columns);
}

bool Grid::isEmptyCell(int x, int y) const {
    return _grid[y][x].getValue() == ' ';
}

bool Grid::isFull()
{
    for (int y = 0; y < _lines; y++)
    {
        for (int x = 0; x < _columns; x++)
        {
            if (_grid[y][x].isEmpty())
            {
                return false;
            }
        }
    }
    return true;
}

void Grid::initArray()
{
    _grid = std::vector<std::vector<Cell>>(_lines, std::vector<Cell>(_columns, Cell()));
}

void Grid::load(const std::vector<std::vector<Cell>>& grid)
{
    for (int y = 0; y < _lines && y < grid.size(); y++)
    {
        const std::vector<Cell> line = grid[y];
        for (int x = 0; x < _columns && x < line.size(); x++)
        {
            const char value = line[x].getValue();
            _grid[y][x].setValue(value);
        }
    }
}
