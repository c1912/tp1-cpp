//
// Created by Fyzoriel on 25/10/2021.
//

#include "Player.h"

Player::Player() : Player('D') {}
Player::Player(char value): _value(value) {}

char Player::getValue() const
{
    return _value;
}
