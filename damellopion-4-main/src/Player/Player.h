//
// Created by Fyzoriel on 25/10/2021.
//

#ifndef DAMELLOPION4_PLAYER_H
#define DAMELLOPION4_PLAYER_H


class Player
{

public:
    Player();
    explicit Player(char value);
    char getValue() const;

private:
    char _value;

};

#endif //DAMELLOPION4_PLAYER_H
