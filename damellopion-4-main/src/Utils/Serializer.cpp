//
// Created by Fyzoriel on 07/12/2021.
//

#include <fstream>
#include <filesystem>
#include <string>

#include "src/Grids/Grid.h"
#include "src/Enums/GamesTypes.h"
#include "Serializer.h"
#include "StringHelper.h"
#include "src/Games/GameFactory.h"

void Serializer::serialize(Game& game, const std::string& fileName)
{
    std::filesystem::create_directories("./saves");
    Grid& gameGrid = game.getGrid();
    std::ofstream file;
    file.open("saves/" + fileName + ".save");
    GAME_TYPE type = game.getType();
    file << type << "," << game.getCurrentPlayer() << std::endl;

    const std::vector<std::vector<Cell>>& grid = gameGrid.getGrid();

    for (int y = 0; y < gameGrid.getLines(); y++)
    {
        for (int x = 0; x < gameGrid.getColumns(); x++)
        {
            file << grid[y][x].getValue();
            if (x < gameGrid.getColumns() - 1)
            {
                file << ",";
            }
            else
            {
                file << std::endl;
            }
        }
    }
    file.close();
}

std::unique_ptr<Game> Serializer::deserialize(const std::string& filePath)
{
    std::ifstream file;
    file.open("saves/" + filePath + ".save");

    std::string line;
    std::getline(file, line);

    std::vector<std::string> splittedLine = StringHelper::split(line, ',');

    if (splittedLine.size() < 2)
    {
        throw std::invalid_argument("Malformed save file");
    }

    int type = std::stoi(splittedLine[0]);
    int currentPlayer = std::stoi(splittedLine[1]);

    std::vector<std::vector<Cell>> grid;

    while (std::getline(file, line))
    {
        splittedLine = StringHelper::split(line, ',');
        std::vector<Cell> cellsLine;
        for (const std::string& str : splittedLine)
        {
            const char value = str.length() > 0 ? str[0] : ' ';

            // Emplace avoid creating temp object
            cellsLine.emplace_back(value);
        }
        grid.push_back(cellsLine);
    }

    file.close();

    std::unique_ptr<Game> game = GameFactory::createGame(type);
    if (game == nullptr)
    {
        throw std::invalid_argument("Invalid game");
    }
    game->load(currentPlayer, grid);

    return game;
}

std::vector<std::string> Serializer::retrieveSaves()
{
    std::vector<std::string> saves;
    for (const auto& entry : std::filesystem::recursive_directory_iterator("./saves"))
    {
        if (entry.path().extension() == ".save")
        {
            saves.push_back(entry.path().stem().string());
        }
    }
    return saves;
}

