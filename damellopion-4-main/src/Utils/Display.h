#ifndef DAMELLOPION4_DISPLAY_H
#define DAMELLOPION4_DISPLAY_H
#include <vector>
#include "src/Cell/Cell.h"
#include"src/Grids/Grid.h"

class Display
{
public:
    static void goodbye();
    static void createOrLoad();
    static void chooseGame();
    static void playAgain();
    static void playerTurn(int player);
    static void draw();
    static void playerWin(int player);
    static void playerWinTokens(int playerWin, int playerWinTokens, int playerLooseTokens);
    static void drawTokens(int player1Score, int player2Score, int neutralScore);
    static void displayGrid(const std::vector<std::vector<Cell>> &grid, int linesMax, int columnsMax);
    static void invalidPosition();
    static void enterValidNumber();
    static void chooseSaveFile(const std::vector<std::string> &saves);

private :
    Display() = default;;
};
#endif //DAMELLOPION4_DISPLAY_H
