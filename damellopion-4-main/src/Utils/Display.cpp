#include <iostream>
#include "Display.h"
#include "Serializer.h"

void Display::goodbye() {
	std::cout << "Goobye !" << std::endl;
}

void Display::chooseGame() {
	std::cout << "1-TicTacToe" << std::endl << "2-Connect 4" << std::endl << "3-Reversi" << std::endl;
	std::cout << "Please select a game : (1, 2 ou 3)" << std::endl;
}

void Display::playAgain() {
	std::cout << "Play again ? (y/n)" << std::endl;
}

void Display::playerTurn(const int player) {
	std::cout << "Player " << player << std::endl;
}

void Display::draw() {
	std::cout << "Draw" << std::endl;
}

void Display::playerWin(const int player) {
	std::cout << "Player " << player << " has won" << std::endl;
}

void Display::playerWinTokens(const int playerWin, const int playerWinTokens, const int playerLooseTokens) {
	std::cout << "Player "<< playerWin << " has won with " << playerWinTokens << " tokens - " << playerLooseTokens << std::endl;
}

void Display::drawTokens(const int player1Score, const int player2Score, const int neutralScore) {
	std::cout << "Draw " << player1Score << " - " << player2Score << " | " << neutralScore << std::endl;
}

void Display::displayGrid(const std::vector<std::vector<Cell>>& grid, const int linesMax, const int columnsMax) {
    char separator = '|';
    std::cout << "   " << separator << " ";
    for (int x = 0; x < columnsMax; x++)
    {
        std::cout << x + 1 << " " << separator << " ";
    }
    std::cout << std::endl;

    for (int y = 0; y < linesMax; y++)
    {
        std::cout << " " << y + 1;
        for (int x = 0; x < columnsMax; x++)
        {
            std::cout << " " << separator << " " << grid[y][x].getValue();
        }
        std::cout << " " << separator << std::endl;
    }
}

void Display::invalidPosition() {
    std::cout << "Please select a valid position" << std::endl;
}

void Display::enterValidNumber() {
    std::cout << "You have to enter a valid number: " << std::endl;
}

void Display::createOrLoad() {
    std::cout << "1-Create a new game" << std::endl << "2-Load a game" << std::endl;
}

void Display::chooseSaveFile(const std::vector<std::string> &saves) {
    std::cout << std::endl <<"Please select a save file to load : " << std::endl;
    for (int i = 0; i < saves.size(); i++)
    {
        std::cout << i + 1 << "-" << saves[i] << std::endl;
    }
}
