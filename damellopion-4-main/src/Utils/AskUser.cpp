//
// Created by Fyzoriel on 22/11/2021.
//

#include <iostream>
#include "AskUser.h"
#include "src/Utils/Display.h"

int AskUser::askNumber(int max)
{
    int ask;
    bool retry;
    do
    {
        std::cin >> ask;
        std::cin.clear();
        std::cin.ignore();
        retry = std::cin.fail() || ask < 1 || ask > max;
        if (retry)
        {
            Display::enterValidNumber();
        }
    } while (retry);

    return ask-1;
}

int AskUser::askCreateOrLoad(){
    Display::createOrLoad();
    return AskUser::askNumber(2);
}

int AskUser::askGameType(){
    Display::chooseGame();
    return AskUser::askNumber(3);
}

std::string AskUser::askFileName(const std::vector<std::string> &saves) {
    Display::chooseSaveFile(saves);
    int saveFileIndex = askNumber(saves.size());
    return saves[saveFileIndex];
}
