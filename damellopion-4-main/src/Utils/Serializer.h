//
// Created by Fyzoriel on 07/12/2021.
//

#ifndef DAMELLOPION4_SERIALIZER_H
#define DAMELLOPION4_SERIALIZER_H


#include <src/Games/Game.h>
#include <memory>

class Serializer
{
public:
    static void serialize(Game& game, const std::string& fileName);
    static std::unique_ptr<Game> deserialize(const std::string& filePath);
    static std::vector<std::string> retrieveSaves();

private:
    Serializer() = default;
};



#endif //DAMELLOPION4_SERIALIZER_H
