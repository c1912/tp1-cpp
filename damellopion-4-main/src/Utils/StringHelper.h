//
// Created by Fyzoriel on 07/12/2021.
//

#ifndef DAMELLOPION4_STRINGHELPER_H
#define DAMELLOPION4_STRINGHELPER_H


#include <vector>

class StringHelper
{
public:
    static std::vector<std::string> split(const std::string& toSplit, char delimiter);

private:
    StringHelper() = default;
};


#endif //DAMELLOPION4_STRINGHELPER_H
