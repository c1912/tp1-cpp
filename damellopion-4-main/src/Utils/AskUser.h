//
// Created by Fyzoriel on 22/11/2021.
//

#ifndef DAMELLOPION4_ASKUSER_H
#define DAMELLOPION4_ASKUSER_H

#include <vector>
#include <string>

class AskUser
{
public:
 /**
 * Ask number input
 * @param max The maximal number
 * @return The inputted number
 */
    static int askNumber(int max);
    static int askCreateOrLoad();
    static int askGameType();
    static std::string askFileName(const std::vector<std::string> &saves);

private:
    AskUser() {};
};


#endif //DAMELLOPION4_ASKUSER_H
