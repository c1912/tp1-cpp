#include "GameTicTacToe.h"
#include "src/Utils/Display.h"
#include "src/Utils/AskUser.h"
#include "src/Utils/Serializer.h"

GameTicTacToe::GameTicTacToe() : _grid(TicTacToeGrid()){}

std::pair<int,int> GameTicTacToe::chooseMove() const
{
    int x, y;
    do
    {
        x = AskUser::askNumber(_grid.getColumns());

        y = AskUser::askNumber(_grid.getLines());
    }
    while (!_grid.isEmptyCell(x, y));

    return std::make_pair(x, y);
}

void GameTicTacToe::start()
{
    bool isFinished = false;
    bool isDraw;

    while(!isFinished)
    {
        Player player = getPlayingPlayer();

        _grid.displayGrid();

        Display::playerTurn(_currentPlayer + 1);

        std::pair<int,int> move = chooseMove();
        _grid.addToken(player, move);

        // win or draw
        if (_grid.isWinner(player) || _grid.isFull())
        {
            isFinished = true;
            isDraw = _grid.isFull();
        }
        else
        {
            changePlayer();
            Serializer::serialize(*this, "latestTicTacToe");
        }
    }

    _grid.displayGrid();

    if (isDraw)
    {
        Display::draw();
    }
    else
    {
        Display::playerWin(_currentPlayer + 1);
    }
    restart();
}

GAME_TYPE GameTicTacToe::getType() const
{
    return GAME_TICTACTOE;
}

Grid& GameTicTacToe::getGrid()
{
    return _grid;
}

void GameTicTacToe::load(int currentPlayer, const std::vector<std::vector<Cell>> &grid)
{
    _currentPlayer = currentPlayer % 2;
    _grid.load(grid);
}


