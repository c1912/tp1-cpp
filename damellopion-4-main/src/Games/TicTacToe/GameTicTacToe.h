#ifndef DAMELLOPION4_GAMETICTACTOE_H
#define DAMELLOPION4_GAMETICTACTOE_H

#include <src/Games/Game.h>
#include "src/Grids/TicTacToe/TicTacToeGrid.h"

class GameTicTacToe : public Game {
public:
    GameTicTacToe();
    void start() override;

    GAME_TYPE getType() const override;
    Grid& getGrid() override;

    void load(int currentPlayer, const std::vector<std::vector<Cell>>& grid) override;

private:
    TicTacToeGrid _grid;
    std::pair<int, int> chooseMove() const;
};


#endif //DAMELLOPION4_GAMETICTACTOE_H
