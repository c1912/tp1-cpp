#include "src/Enums/GamesTypes.h"
#include <src/Games/TicTacToe/GameTicTacToe.h>
#include <src/Games/Connect4/GameConnect4.h>
#include <src/Games/Reversi/GameReversi.h>
#include "GameFactory.h"

std::unique_ptr<Game> GameFactory::createGame(const int gameType)
{
    switch (gameType)
    {
        case GAME_TICTACTOE:
            return std::make_unique<GameTicTacToe>();
        case GAME_CONNECT4:
            return std::make_unique<GameConnect4>();
        case GAME_REVERSI:
            return std::make_unique<GameReversi>();
        default:
            return nullptr;
    }
}
