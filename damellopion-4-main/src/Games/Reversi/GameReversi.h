//
// Created by Fyzoriel on 23/11/2021.
//

#ifndef DAMELLOPION4_GAMEREVERSI_H
#define DAMELLOPION4_GAMEREVERSI_H

#include <src/Games/Game.h>
#include "src/Grids/Reversi/ReversiGrid.h"

class GameReversi : public Game
{
public:
    GameReversi();
    void start() override;
    GAME_TYPE getType() const override;
    Grid& getGrid() override;

    void load(int currentPlayer, const std::vector<std::vector<Cell>>& grid) override;

private:
    ReversiGrid _grid;
};


#endif //DAMELLOPION4_GAMEREVERSI_H
