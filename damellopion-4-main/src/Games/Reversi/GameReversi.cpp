//
// Created by Fyzoriel on 23/11/2021.
//

#include <iostream>
#include "GameReversi.h"
#include "src/Utils/Display.h"
#include "src/Utils/Serializer.h"

GameReversi::GameReversi() : _grid(ReversiGrid(_players[0].getValue(), _players[1].getValue())){}

/**
 * Start reversi game
 */
void GameReversi::start()
{
    bool isFinished = false;
    bool hasAlreadySkip = false;

    do
    {
        // Get playing player
        Player player = getPlayingPlayer();

        if (!_grid.canPlay(player))
        {
            if (hasAlreadySkip)
            {
                break;
            }
            hasAlreadySkip = true;
            changePlayer();
            continue;
        }

        // Display board
        _grid.displayGrid();

        Display::playerTurn(_currentPlayer + 1);

        // Add token
        _grid.addToken(player);

        // win or draw
        if (_grid.isFull())
        {
            isFinished = true;
        }
        else
        {
            changePlayer();
            Serializer::serialize(*this, "latestReversi");
        }
    } while(!isFinished);

    // Display board
    _grid.displayGrid();

    int scorePlayer1, scorePlayer2, scoreNeutral;

    _grid.getScores(scorePlayer1, scorePlayer2, scoreNeutral);

    // Display draw or player who won
    if (scorePlayer1 > scorePlayer2)
    {
        Display::playerWinTokens(1, scorePlayer1 + scoreNeutral, scorePlayer2);
    }
    else if (scorePlayer1 < scorePlayer2)
    {
        Display::playerWinTokens(2, scorePlayer2 + scoreNeutral, scorePlayer1);
    }
    else
    {
        Display::drawTokens(scorePlayer1, scorePlayer2, scoreNeutral);
    }
    restart();
}

GAME_TYPE GameReversi::getType() const
{
    return GAME_REVERSI;
}

Grid &GameReversi::getGrid()
{
    return _grid;
}

void GameReversi::load(int currentPlayer, const std::vector<std::vector<Cell>> &grid)
{
    _currentPlayer = currentPlayer % 2;
    _grid.load(grid);
}
