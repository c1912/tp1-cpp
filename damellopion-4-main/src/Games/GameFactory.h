#ifndef DAMELLOPION4_GAMEFACTORY_H
#define DAMELLOPION4_GAMEFACTORY_H

#include <memory>
#include "Game.h"

class GameFactory
{
public:
    static std::unique_ptr<Game> createGame(int gameType);
};


#endif //DAMELLOPION4_GAMEFACTORY_H
