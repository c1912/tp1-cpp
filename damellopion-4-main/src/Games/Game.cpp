//
// Created by Fyzoriel on 13/10/2021.
//

#include <iostream>
#include "Game.h"
#include "src/Utils/Display.h"

Game::Game()
{
    _players[0] = Player('X');
    _players[1] = Player('O');
    _currentPlayer = 0;
}

void Game::restart()
{
    char ask;
    Display::playAgain();
    do
    {
        std::cin >> ask;

    } while (ask != 'y' && ask != 'n');
    if (ask == 'y')
    {
        _currentPlayer = 0;
        getGrid().initArray();
        start();
    }
}


const Player& Game::getPlayingPlayer() const
{
    return _players[_currentPlayer];
}

void Game::changePlayer()
{
    _currentPlayer = (_currentPlayer + 1) % 2;
}

int Game::getCurrentPlayer() const
{
    return _currentPlayer;
}

