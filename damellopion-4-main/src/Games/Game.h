//
// Created by Fyzoriel on 13/10/2021.
//

#ifndef DAMELLOPION4_GAME_H
#define DAMELLOPION4_GAME_H

#include <iostream>
#include "src/Enums/GamesTypes.h"
#include "src/Grids/Grid.h"

class Game
{
public:
    Game();
    virtual void start() = 0;
    virtual GAME_TYPE getType() const = 0;
    virtual Grid& getGrid() = 0;
 /**
 * Get the playing player
 * @return the playing player
 */
    const Player& getPlayingPlayer() const;
    int getCurrentPlayer() const;

    virtual void load(int currentPlayer, const std::vector<std::vector<Cell>>& grid) = 0;

protected:
    Player _players[2] = {};
    int _currentPlayer;
 /**
 * Ask for restart the game
 */
    void restart();
 /**
 * Change the playing player
 */
    void changePlayer();
};

#endif //DAMELLOPION4_GAME_H
