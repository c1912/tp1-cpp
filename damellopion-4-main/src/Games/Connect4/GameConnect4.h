#ifndef DAMELLOPION4_GAMECONNECT4_H
#define DAMELLOPION4_GAMECONNECT4_H


#include "src/Games/Game.h"
#include "src/Grids/Connect4/Connect4Grid.h"

class GameConnect4 : public Game
{
public :
    GameConnect4();
    void start() override;
    GAME_TYPE getType() const override;
    Grid& getGrid() override;

    void load(int currentPlayer, const std::vector<std::vector<Cell>>& grid) override;


private :
    Connect4Grid _grid;
    std::pair<int,int> chooseColumn() const;
};


#endif //DAMELLOPION4_GAMECONNECT4_H
