#include "GameConnect4.h"
#include "src/Utils/Display.h"
#include "src/Utils/AskUser.h"
#include "src/Utils/Serializer.h"

GameConnect4::GameConnect4() : _grid(Connect4Grid()) {}

std::pair<int, int> GameConnect4::chooseColumn() const
{
    bool isCorrectPos = false;
    int x, y;

    while (!isCorrectPos)
    {
        x = AskUser::askNumber(_grid.getColumns());

        if (_grid.hasEmptyCell(x)){
            isCorrectPos = true;
            y = _grid.getFirstEmptyCell(x);
        }
        else {
            //TODO: alert player that column is full
        }
    }

    return std::make_pair(x, y);
}

void GameConnect4::start()
{
    bool isFinished = false;
    bool isDraw = false;

    while(!isFinished)
    {
        Player player = getPlayingPlayer();

        _grid.displayGrid();

        Display::playerTurn(_currentPlayer + 1);

        std::pair<int,int> move = chooseColumn();
        _grid.addToken(player, move);

        // win or draw
        if (_grid.isWinner(player) || _grid.isFull())
        {
            isFinished = true;
            isDraw = _grid.isFull();
        }
        else
        {
            changePlayer();
            Serializer::serialize(*this, "latestConnect4");
        }
    }

    _grid.displayGrid();

    if (isDraw)
    {
        Display::draw();
    }
    else
    {
        Display::playerWin(_currentPlayer + 1);
    }
    restart();
}

GAME_TYPE GameConnect4::getType() const
{
    return GAME_CONNECT4;
}

Grid &GameConnect4::getGrid()
{
    return _grid;
}

void GameConnect4::load(int currentPlayer, const std::vector<std::vector<Cell>> &grid)
{
    _currentPlayer = currentPlayer % 2;
    _grid.load(grid);
}



