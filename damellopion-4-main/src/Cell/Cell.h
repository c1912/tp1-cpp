//
// Created by Fyzoriel on 25/10/2021.
//

#ifndef DAMELLOPION4_CELL_H
#define DAMELLOPION4_CELL_H


#include "src/Player/Player.h"

class Cell
{

public:
    Cell();
    Cell(char value);
 /**
 * Check if cell is empty
 * @return true if cell is empty
 */
    bool isEmpty() const;
    void setValue(char value);
    char getValue() const;

private:
    char _value;
};


#endif //DAMELLOPION4_CELL_H
