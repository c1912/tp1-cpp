//
// Created by Fyzoriel on 25/10/2021.
//

#include "Cell.h"

Cell::Cell(): _value(' ') {}

Cell::Cell(char value): _value(value) {}

bool Cell::isEmpty() const
{
    return _value == ' ';
}

void Cell::setValue(char value)
{
    _value = value;
}

char Cell::getValue() const
{
    return _value;
}
