#include "Grids/Connect4/Connect4Grid.h"
#include "Utils/AskUser.h"
#include "Utils/Display.h"
#include "Utils/Serializer.h"
#include "src/Games/GameFactory.h"

int main()
{
    std::unique_ptr<Game> game;

    int createOrLoadGame = AskUser::askCreateOrLoad();
    if (createOrLoadGame == 0){
        int gameType = AskUser::askGameType();
        game = GameFactory::createGame(gameType);
    }
    else {
        const std::vector<std::string> saves = Serializer::retrieveSaves();
        std::string fileName = AskUser::askFileName(saves);
        game = Serializer::deserialize(fileName);
    }

    game->start();

    Display::goodbye();
    return 0;
}
