#include <iostream>
#include <conio.h>
#include <conio2.h>
#include <time.h>

using namespace std;

int nbralea, nbrj, essai;

int main()
{
    srand(time(NULL));
    nbralea = rand()%1001;
    cout << "Devinez le nombre entre 0 et 1000 : ";
    cin >> nbrj;
    essai++;
    while(nbrj!=nbralea)
    {
        if (nbrj<nbralea)
        {
            essai++;
            clrscr();
            cout << "La valeur est plus grande que : " << nbrj << ", veuillez entrer un autre nombre : ";
            cin >> nbrj;
        }
        if (nbrj>nbralea)
        {
            essai++;
            clrscr();
            cout << "La valeur est plus petite que : " << nbrj << ", veuillez entrer un autre nombre : ";
            cin >> nbrj;
        }
    }
    clrscr();
    cout << "Bravo! Vous avez devine le nombre en " << essai << " essais." << endl;

    return 0;
}
